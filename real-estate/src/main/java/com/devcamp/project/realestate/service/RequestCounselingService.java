package com.devcamp.project.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.project.realestate.model.CRequestCounseling;
import com.devcamp.project.realestate.repository.IRequestCounselingRepository;

@Service
public class RequestCounselingService {
    @Autowired
    IRequestCounselingRepository pCounselingRepository;

    public List<CRequestCounseling> getAllReqCounseling() {
        return pCounselingRepository.findAll();
    }

    public Object createNewReqCounseling(CRequestCounseling cRequestCounseling) {
        CRequestCounseling requestCounseling = new CRequestCounseling();
        requestCounseling.setFullname(cRequestCounseling.getFullname());
        requestCounseling.setEmail(cRequestCounseling.getEmail());
        requestCounseling.setPhoneNum(cRequestCounseling.getPhoneNum());
        requestCounseling.setMessage(cRequestCounseling.getMessage());
        requestCounseling.setStatus("N");
        CRequestCounseling saveReq = pCounselingRepository.save(requestCounseling);
        return saveReq;
    }

    public Object updateReqCounseling(int id) {
        Optional<CRequestCounseling> rOptional = pCounselingRepository.findById(id);
        if (rOptional.isPresent()) {
            CRequestCounseling requestCounseling = rOptional.get();
            requestCounseling.setStatus("Y");
            CRequestCounseling saveReq = pCounselingRepository.save(requestCounseling);
            return saveReq;
        } else {
            return null;
        }
    }

    public void deleteReqCounseling(int id) {
        pCounselingRepository.deleteById(id);
    }
}
