package com.devcamp.project.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.project.realestate.model.CRequestCounseling;

@Repository
public interface IRequestCounselingRepository extends JpaRepository<CRequestCounseling, Integer> {
    @Query(value = "SELECT * FROM `request_counseling` WHERE `status` = 'N'", nativeQuery = true)
    List<CRequestCounseling> getRequestCounselingByStatus();
}
