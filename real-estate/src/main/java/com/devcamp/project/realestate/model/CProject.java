package com.devcamp.project.realestate.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "project")
public class CProject {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name")
    private String name;

    @ManyToOne
    @JsonIgnore
    private CProvince _province;

    @ManyToOne
    private CDistrict _district;

    @ManyToOne
    @JsonIgnore
    private CWard _ward;

    @ManyToOne
    @JsonIgnore
    private CStreet _street;

    @Column(name = "address", length = 1000)
    private String address;

    @Column(name = "slogan", columnDefinition="mediumtext")
    private String slogan;

    @Column(name = "description", columnDefinition="text")
    private String description;

    @Column(name = "acreage", columnDefinition="decimal(20,2)")
    private Double acreage;

    @Column(name = "construct_area", columnDefinition="decimal(20,2)")
    private Double constructArea;

    @Column(name = "num_block")
    private Integer numBlock;
    
    @Column(name = "num_floors", length = 500)
    private String numFloors;
    
    @Column(name = "num_apartment", nullable = false)
    private Integer numApartment;
    
    @Column(name = "apartmentt_area", length = 500)
    private String apartmenttArea;

    @ManyToOne
    private CInvestor investor;

    @ManyToOne
    private CConstructionContractor constructionContractor;
        
    @Column(name = "design_unit")
    private Integer designUnit;

    @Column(name = "utilities", length = 1000, nullable = false)
    private String utilities;

    @Column(name = "region_link", length = 1000, nullable = false)
    private String regionLink;

    @Column(name = "photo", length = 5000)
    private String photo;

    @Column(name = "_lat")
    private Double _lat;

    @Column(name = "_lng")
    private Double _lng;

    @OneToMany(targetEntity = CRealEstate.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id")
    @JsonIgnore
    private Set<CRealEstate> realestates;

    @OneToMany(targetEntity = CMasterLayout.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id")
    @JsonIgnore
    private Set<CMasterLayout> masterLayouts;

    public CProject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public Double getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(Double constructArea) {
        this.constructArea = constructArea;
    }

    public Integer getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Integer numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmenttArea() {
        return apartmenttArea;
    }

    public void setApartmenttArea(String apartmenttArea) {
        this.apartmenttArea = apartmenttArea;
    }

    public CWard get_ward() {
        return _ward;
    }

    public void set_ward(CWard _ward) {
        this._ward = _ward;
    }

    public CStreet get_street() {
        return _street;
    }

    public void set_street(CStreet _street) {
        this._street = _street;
    }

    public CInvestor getInvestor() {
        return investor;
    }

    public void setInvestor(CInvestor investor) {
        this.investor = investor;
    }

    public Set<CRealEstate> getRealestates() {
        return realestates;
    }

    public void setRealestates(Set<CRealEstate> realestates) {
        this.realestates = realestates;
    }

    public CConstructionContractor getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(CConstructionContractor constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public Integer getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double get_lat() {
        return _lat;
    }

    public void set_lat(Double _lat) {
        this._lat = _lat;
    }

    public Double get_lng() {
        return _lng;
    }

    public void set_lng(Double _lng) {
        this._lng = _lng;
    }

    public CProvince get_province() {
        return _province;
    }

    public void set_province(CProvince _province) {
        this._province = _province;
    }

    public CDistrict get_district() {
        return _district;
    }

    public void set_district(CDistrict _district) {
        this._district = _district;
    }


}
