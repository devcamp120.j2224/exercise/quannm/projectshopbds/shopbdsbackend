package com.devcamp.project.realestate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project.realestate.model.CRequestCounseling;
import com.devcamp.project.realestate.repository.IRequestCounselingRepository;
import com.devcamp.project.realestate.service.RequestCounselingService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class RequestCounselingController {
    @Autowired
    RequestCounselingService pCounselingService;
    @Autowired
    IRequestCounselingRepository pCounselingRepository;

    @GetMapping("/reqCounseling")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> getAllReqCounseling() {
        if (!pCounselingService.getAllReqCounseling().isEmpty()) {
            return new ResponseEntity<>(pCounselingService.getAllReqCounseling(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/reqCounselingStatusN")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> getRequestCounselingByStatus() {
        if (!pCounselingRepository.getRequestCounselingByStatus().isEmpty()) {
            return new ResponseEntity<>(pCounselingRepository.getRequestCounselingByStatus(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/reqCounseling")
    public ResponseEntity<Object> createNewReqCounseling(@RequestBody CRequestCounseling cRequestCounseling) {
        try {
            return new ResponseEntity<>(pCounselingService.createNewReqCounseling(cRequestCounseling), HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Request Counseling: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/reqCounseling/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> updateReqCounseling(@PathVariable int id) {
        if (pCounselingService.updateReqCounseling(id) != null) {
            try {
                return new ResponseEntity<>(pCounselingService.updateReqCounseling(id), HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Request Counseling: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/reqCounseling/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> deleteReqCounselingById(@PathVariable int id) {
        try {
            pCounselingService.deleteReqCounseling(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
