package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.CInvestor;
import org.springframework.stereotype.Repository;

@Repository
public interface IInvestorRepository extends JpaRepository<CInvestor, Integer> {
    
}
