package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.ILocationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationService {
    @Autowired
    ILocationRepository pLocationRepository;

    public List<CLocation> getLocationList() {
        List<CLocation> locationList = new ArrayList<CLocation>();
        pLocationRepository.findAll().forEach(locationList::add);
        return locationList;
    }

    public Object createLocationObj(CLocation cLocation) {

        CLocation newLocation = new CLocation();
        newLocation.setLatitude(cLocation.getLatitude());
        newLocation.setLongitude(cLocation.getLongitude());

        CLocation savedLocation = pLocationRepository.save(newLocation);
        return savedLocation;
    }

    public Object updateLocationObj(CLocation cLocation, Optional<CLocation> cLocationData) {

        CLocation newLocation = cLocationData.get();
        newLocation.setLatitude(cLocation.getLatitude());
        newLocation.setLongitude(cLocation.getLongitude());

        CLocation savedLocation = pLocationRepository.save(newLocation);
        return savedLocation;
    }
}
